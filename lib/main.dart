import 'package:dialog_box/TabletDialog.dart';
import 'package:flutter/material.dart';


void main() => runApp(MaterialApp(home: MainPage(),
  debugShowCheckedModeBanner: false));


class MainPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.teal[600],
      ),
      body: Center(
        child: RaisedButton(
          child: Text("Show Dialog"),
          onPressed: () => _showDialog(context),
        ),
      ),
    );
  }
}



  Future _showDialog(BuildContext context) async {
    await showDialog<Null>(
      context: context,
      builder: (BuildContext context){

        bool isLargeScreen = false;

        if(MediaQuery.of(context).size.width > 530) {
          isLargeScreen = true;
        } else {
          isLargeScreen = false;
        }
        return isLargeScreen ? TabletDialog() : Dialog();
      }
    );
  }



class Dialog extends StatefulWidget {


  @override
  DialogState createState() {
    return new DialogState();
  }
}

class DialogState extends State<Dialog> {

  DateTime _date = DateTime.now();

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: _date,
      firstDate: DateTime(2017),
      lastDate: DateTime(2020),
    );
    if (picked != null && picked != _date) {
      print(_date.toString());
      setState(() {
        _date = picked;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      content: Stack(
        alignment: Alignment.topLeft,
        children: <Widget>[

          //Close Button
          GestureDetector(
            onTap: () => Navigator.pop(context),
            child: CircleAvatar(
              maxRadius: 12,
              backgroundColor: Colors.red[600],
              child: Icon(
                Icons.close,
                color: Colors.white,
                size: 15,
              ),
            ),
          ),
          Container(
            height: MediaQuery.of(context).size.height > 700 ? 520 :  410,
            width: MediaQuery.of(context).size.width,
            child: Column(
              children: <Widget>[

                Text("تقييم مستوى الطالب"),

                //Stars
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(Icons.star, color: Colors.orangeAccent),
                    Icon(Icons.star, color: Colors.orangeAccent),
                    Icon(Icons.star, color: Colors.orangeAccent),
                    Icon(Icons.star, color: Colors.orangeAccent),
                    Icon(Icons.star,color: Colors.black54),
                  ],
                ),

                SizedBox(height: 10),

                //المصحف وتسجيل الصوت و التسميع اون لاين
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[

                    //تسميع اون لاين
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Container(
                          height: 60,
                          width: 70,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              SizedBox(height: 5),
                              Icon(Icons.record_voice_over,color: Colors.grey,),
                              Text("تسميع اون لاين",style: TextStyle(
                                  color: Colors.grey,
                                  fontSize: 11
                              ),)
                            ],
                          ),
                        )
                      ],
                    ),

                    //تسجيل الصوت
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Container(
                          height: 60,
                          width: 70,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              SizedBox(height: 5),
                              Icon(Icons.settings_voice,color: Colors.grey,),
                              Text("تسجيل الصوت",style: TextStyle(
                                  color: Colors.grey,
                                  fontSize: 11
                              ),)
                            ],
                          ),
                        )
                      ],
                    ),

                    //المصحف
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Container(
                          height: 60,
                          width: 60,
                          color: Colors.teal[600],
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              SizedBox(height: 5),
                              Icon(Icons.book,color: Colors.white,),
                              Text("المصحف",style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 11
                              ),)
                            ],
                          ),
                        )
                      ],
                    ),
                  ],
                ),

                SizedBox(height: 20),

                //تعديل وايقاف الخطة
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[

                    //إيقاف الخطة
                    Row(
                      children: <Widget>[
                        Text("إيقاف الخطة",style: TextStyle(
                            color: Colors.grey[600],
                            fontSize: 11
                        ),),
                        SizedBox(width: 2),
                        Icon(
                          Icons.pause,
                          color: Colors.red,
                          size: 11,
                        ),
                      ],
                    ),

                    //تعديل الخطة
                    Row(
                      children: <Widget>[
                        Text("تعديل الخطة",style: TextStyle(
                            color: Colors.grey[600],
                            fontSize: 11
                        ),),
                        SizedBox(width: 2),
                        Icon(
                          Icons.mode_edit,
                          color: Colors.grey[600],
                          size: 11,
                        ),
                      ],
                    ),

                    //تعديل المقرر
                    Row(
                      children: <Widget>[
                        Text("تعديل المقرر",style: TextStyle(
                            color: Colors.teal[600],
                            fontSize: 11
                        ),),
                        SizedBox(width: 2),
                        Icon(
                          Icons.system_update_alt,
                          color: Colors.teal[600],
                          size: 11,
                        ),
                      ],
                    ),
                  ],
                ),

                Divider(color: Colors.black87),

                //Calendar
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[

                    //السنة
                    Container(
                      height: 35,
                      width: 60,
                      color: Colors.grey[200],
                      child: Stack(
                        alignment: Alignment.topRight,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(right: 2.0),
                            child: Text("سنة",style: TextStyle(
                                fontSize: 11,
                                color: Colors.black54
                            ),),
                          ),
                          Container(
                            alignment: Alignment.center,
                            padding: EdgeInsets.only(right: 5),
                            child: Text(
                              "${_date.year.toString()}",
                              style: TextStyle(
                                  fontSize: 11,
                                  color: Colors.black54
                              ),
                            ),
                          )
                        ],
                      ),
                    ),

                    //الشهر
                    Container(
                      height: 35,
                      width: 60,
                      color: Colors.grey[200],
                      child: Stack(
                        alignment: Alignment.topRight,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(right: 3.0),
                            child: Text("شهر",style: TextStyle(
                                fontSize: 11,
                                color: Colors.black54
                            ),),
                          ),
                          Container(
                            alignment: Alignment.center,
                            padding: EdgeInsets.only(right: 5),
                            child: Text(
                              "${_date.month.toString()}",
                              style: TextStyle(
                                  fontSize: 13,
                                  color: Colors.black54
                              ),
                            ),
                          )
                        ],
                      ),
                    ),

                    //اليوم
                    Container(
                      height: 35,
                      width: 60,
                      color: Colors.grey[200],
                      child: Stack(
                        alignment: Alignment.topRight,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(right: 2.0),
                            child: Text("اليوم",style: TextStyle(
                                fontSize: 11,
                                color: Colors.black54

                            ),),
                          ),
                          Container(
                            alignment: Alignment.center,
                            padding: EdgeInsets.only(right: 6),
                            child: Text(
                              "${_date.day.toString()}",
                              style: TextStyle(
                                  fontSize: 11,
                                  color: Colors.black54
                              ),
                            ),
                          )
                        ],
                      ),
                    ),

                    SizedBox(width: 3),

                    GestureDetector(
                        onTap: () => _selectDate(context),


                        child: Icon(Icons.calendar_today,color: Colors.teal[600])),

                  ],
                ),

                SizedBox(
                  height: 25,
                ),

                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[

                    //Plus And Minus
                    Column(
                      children: <Widget>[

                        Container(
                          height: 35,
                          width: 85,
                          color: Colors.grey[200],
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[

                              Padding(
                                padding: const EdgeInsets.only(left: 2.0),
                                child: Icon(Icons.add_circle_outline,color: Colors.blueGrey[200]),
                              ),

                              Text("12",style: TextStyle(
                                color: Colors.black87,
                                fontSize: 15,
                              ),),

                              Padding(
                                padding: const EdgeInsets.only(right: 2.0),
                                child: Icon(Icons.remove_circle_outline,color: Colors.blueGrey[200]),
                              ),


                            ],
                          ),
                        ),

                        SizedBox(height: 22),

                        Container(
                          height: 35,
                          width: 85,
                          color: Colors.grey[200],
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[

                              Padding(
                                padding: const EdgeInsets.only(left: 2.0),
                                child: Icon(Icons.add_circle_outline,color: Colors.blueGrey[200]),
                              ),

                              Text("35",style: TextStyle(
                                color: Colors.black87,
                                fontSize: 15,
                              ),),

                              Padding(
                                padding: const EdgeInsets.only(right: 2.0),
                                child: Icon(Icons.remove_circle_outline,color: Colors.blueGrey[200]),
                              ),


                            ],
                          ),
                        ),
                      ],
                    ),

                    SizedBox(width: 15),

                    //السور
                    Column(
                      children: <Widget>[

                        //سورة المجادلة
                        Container(
                          height: 35,
                          width: 85,
                          color: Colors.grey[200],
                          child: Stack(
                            alignment: Alignment.topRight,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(right: 3.0),
                                child: Text("من",style: TextStyle(
                                    fontSize: 10,
                                    color: Colors.black54
                                ),),
                              ),
                              Center(
                                child: Padding(
                                  padding: const EdgeInsets.only(top: 2.0),
                                  child: Text("سورة المجادلة",
                                    style: TextStyle(
                                        fontSize: 12
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),

                        SizedBox(height: 22),

                        //سورة الحشر
                        Container(
                          height: 35,
                          width: 85,
                          color: Colors.grey[200],
                          child: Stack(
                            alignment: Alignment.topRight,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(right: 3.0),
                                child: Text("إلى",style: TextStyle(
                                    fontSize: 10,
                                    color: Colors.black54
                                ),),
                              ),
                              Center(
                                child: Padding(
                                  padding: const EdgeInsets.only(top: 2.0),
                                  child: Text("سورة الحشر",
                                    style: TextStyle(
                                        fontSize: 12
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    ),

                    SizedBox(width: 7),

                    //Black Circle Avatars
                    Column(
                      children: <Widget>[
                        SizedBox(height: 10),

                        CircleAvatar(
                          maxRadius: 4,
                          backgroundColor: Colors.black54,
                        ),

                        SizedBox(height: 7),

                        CircleAvatar(
                          maxRadius: 2,
                          backgroundColor: Colors.black45,
                        ),
                        SizedBox(height: 5),
                        CircleAvatar(
                          maxRadius: 2,
                          backgroundColor: Colors.black45,
                        ),
                        SizedBox(height: 5),
                        CircleAvatar(
                          maxRadius: 2,
                          backgroundColor: Colors.black45,
                        ),
                        SizedBox(height: 5),
                        CircleAvatar(
                          maxRadius: 2,
                          backgroundColor: Colors.black45,
                        ),
                        SizedBox(height: 5),
                        CircleAvatar(
                          maxRadius: 2,
                          backgroundColor: Colors.black45,
                        ),

                        SizedBox(height: 7),

                        CircleAvatar(
                          maxRadius: 4,
                          backgroundColor: Colors.black54,
                        ),
                      ],
                    ),

                    SizedBox(width: 7)

                  ],
                ),

                SizedBox(height: 7),

                Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    RaisedButton(
                        color: Colors.teal[600],
                        child: Text("حفظ",style: TextStyle(
                          color: Colors.white,
                          fontSize: 17,
                        ),),
                        onPressed: () {}
                    ),
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}

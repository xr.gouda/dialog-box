import 'package:flutter/material.dart';



class TabletDialog extends StatefulWidget {


  @override
  TabletDialogState createState() {
    return new TabletDialogState();
  }
}

class TabletDialogState extends State<TabletDialog> {

  DateTime _date = DateTime.now();

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: _date,
      firstDate: DateTime(2017),
      lastDate: DateTime(2020),
    );
    if (picked != null && picked != _date) {
      print(_date.toString());
      setState(() {
        _date = picked;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      content: Stack(
        alignment: Alignment.topLeft,
        children: <Widget>[

          //Close Button
          GestureDetector(
            onTap: () => Navigator.pop(context),
            child: CircleAvatar(
              maxRadius: 15,
              backgroundColor: Colors.red[600],
              child: Icon(
                Icons.close,
                color: Colors.white,
                size: 21,
              ),
            ),
          ),


          Container(
            height: MediaQuery.of(context).size.height > 700 ? 520 :  410,
            width: MediaQuery.of(context).size.width,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[

                Text("تقييم مستوى الطالب"),

                //Stars
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(Icons.star, color: Colors.orangeAccent),
                    Icon(Icons.star, color: Colors.orangeAccent),
                    Icon(Icons.star, color: Colors.orangeAccent),
                    Icon(Icons.star, color: Colors.orangeAccent),
                    Icon(Icons.star,color: Colors.black54),
                  ],
                ),

                SizedBox(height: 10),

                //المصحف وتسجيل الصوت و التسميع اون لاين
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[

                    //تسميع اون لاين
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Container(
                          height: 80,
                          width: 90,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              SizedBox(height: 10),
                              Icon(Icons.record_voice_over,color: Colors.grey,),
                              SizedBox(height: 3),
                              Text("تسميع اون لاين",style: TextStyle(
                                  color: Colors.grey,
                                  fontSize: 11
                              ),)
                            ],
                          ),
                        )
                      ],
                    ),

                    //تسجيل الصوت
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Container(
                            height: 80,
                            width: 90,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              SizedBox(height: 10),
                              Icon(Icons.settings_voice,color: Colors.grey,),
                              SizedBox(height: 3,),
                              Text("تسجيل الصوت",style: TextStyle(
                                  color: Colors.grey,
                                  fontSize: 11
                              ),)
                            ],
                          ),
                        )
                      ],
                    ),

                    //المصحف
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Container(
                          height: 80,
                          width: 90,
                          color: Colors.teal[600],
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              SizedBox(height: 5),
                              Icon(Icons.book,color: Colors.white,),
                              Text("المصحف",style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 11
                              ),)
                            ],
                          ),
                        )
                      ],
                    ),
                  ],
                ),

                SizedBox(height: 20),

                //تعديل وايقاف الخطة
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[

                    //إيقاف الخطة
                    Row(
                      children: <Widget>[
                        Text("إيقاف الخطة",style: TextStyle(
                            color: Colors.grey[600],
                            fontSize: 15
                        ),),
                        SizedBox(width: 2),
                        Icon(
                          Icons.pause,
                          color: Colors.red,
                        ),
                      ],
                    ),

                    //تعديل الخطة
                    Row(
                      children: <Widget>[
                        Text("تعديل الخطة",style: TextStyle(
                            color: Colors.grey[600],
                            fontSize: 15
                        ),),
                        SizedBox(width: 2),
                        Icon(
                          Icons.mode_edit,
                          color: Colors.grey[600],
                        ),
                      ],
                    ),

                    //تعديل المقرر
                    Row(
                      children: <Widget>[
                        Text("تعديل المقرر",style: TextStyle(
                            color: Colors.teal[600],
                            fontSize: 15
                        ),),
                        SizedBox(width: 2),
                        Icon(
                          Icons.system_update_alt,
                          color: Colors.teal[600],
                        ),
                      ],
                    ),
                  ],
                ),

                Divider(color: Colors.black87),

                //Calendar
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[

                    //السنة
                    Container(
                      height: 45,
                      width: 75,
                      color: Colors.grey[200],
                      child: Stack(
                        alignment: Alignment.topRight,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(right: 2.0),
                            child: Text("سنة",style: TextStyle(
                                fontSize: 12,
                                color: Colors.black54
                            ),),
                          ),
                          Container(
                            alignment: Alignment.center,
                            padding: EdgeInsets.only(right: 5),
                            child: Text(
                              "${_date.year.toString()}",
                              style: TextStyle(
                                  fontSize: 13,
                                  color: Colors.black54
                              ),
                            ),
                          )
                        ],
                      ),
                    ),

                    SizedBox(width: 30),

                    //الشهر
                    Container(
                      height: 45,
                      width: 75,
                      color: Colors.grey[200],
                      child: Stack(
                        alignment: Alignment.topRight,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(right: 3.0),
                            child: Text("شهر",style: TextStyle(
                                fontSize: 12,
                                color: Colors.black54
                            ),),
                          ),
                          Container(
                            alignment: Alignment.center,
                            padding: EdgeInsets.only(right: 5),
                            child: Text(
                              "${_date.month.toString()}",
                              style: TextStyle(
                                  fontSize: 13,
                                  color: Colors.black54
                              ),
                            ),
                          )
                        ],
                      ),
                    ),

                    SizedBox(width: 30),


                    //اليوم
                    Container(
                      height: 45,
                      width: 75,
                      color: Colors.grey[200],
                      child: Stack(
                        alignment: Alignment.topRight,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(right: 2.0),
                            child: Text("اليوم",style: TextStyle(
                                fontSize: 12,
                                color: Colors.black54

                            ),),
                          ),
                          Container(
                            alignment: Alignment.center,
                            padding: EdgeInsets.only(right: 6),
                            child: Text(
                              "${_date.day.toString()}",
                              style: TextStyle(
                                  fontSize: 13,
                                  color: Colors.black54
                              ),
                            ),
                          )
                        ],
                      ),
                    ),

                    SizedBox(width: 30),


                    GestureDetector(
                        onTap: () => _selectDate(context),


                        child: Icon(Icons.calendar_today,color: Colors.teal[600])),

                  ],
                ),

                SizedBox(
                  height: 25,
                ),

                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[

                    //Plus And Minus
                    Column(
                      children: <Widget>[

                        Container(
                          height: 43,
                          width: 100,
                          color: Colors.grey[200],
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: <Widget>[

                              Padding(
                                padding: const EdgeInsets.only(left: 2.0),
                                child: Icon(Icons.add_circle_outline,color: Colors.blueGrey[200]),
                              ),

                              Text("12",style: TextStyle(
                                color: Colors.black87,
                                fontSize: 15,
                              ),),

                              Padding(
                                padding: const EdgeInsets.only(right: 2.0),
                                child: Icon(Icons.remove_circle_outline,color: Colors.blueGrey[200]),
                              ),


                            ],
                          ),
                        ),

                        SizedBox(height: 20),

                        Container(
                          height: 43,
                          width: 100,
                          color: Colors.grey[200],
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[

                              Padding(
                                padding: const EdgeInsets.only(left: 2.0),
                                child: Icon(Icons.add_circle_outline,color: Colors.blueGrey[200]),
                              ),

                              Text("35",style: TextStyle(
                                color: Colors.black87,
                                fontSize: 15,
                              ),),

                              Padding(
                                padding: const EdgeInsets.only(right: 2.0),
                                child: Icon(Icons.remove_circle_outline,color: Colors.blueGrey[200]),
                              ),


                            ],
                          ),
                        ),
                      ],
                    ),

                    SizedBox(width: 50),

                    //السور
                    Column(
                      children: <Widget>[

                        //سورة المجادلة
                        Container(
                          height: 43,
                          width: 100,
                          color: Colors.grey[200],
                          child: Stack(
                            alignment: Alignment.topRight,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(right: 3.0),
                                child: Text("من",style: TextStyle(
                                    fontSize: 10,
                                    color: Colors.black54
                                ),),
                              ),
                              Center(
                                child: Padding(
                                  padding: const EdgeInsets.only(top: 2.0),
                                  child: Text("سورة المجادلة",
                                    style: TextStyle(
                                        fontSize: 12
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),

                        SizedBox(height: 20),

                        //سورة الحشر
                        Container(
                          height: 43,
                          width: 100,
                          color: Colors.grey[200],
                          child: Stack(
                            alignment: Alignment.topRight,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(right: 3.0),
                                child: Text("إلى",style: TextStyle(
                                    fontSize: 10,
                                    color: Colors.black54
                                ),),
                              ),
                              Center(
                                child: Padding(
                                  padding: const EdgeInsets.only(top: 2.0),
                                  child: Text("سورة الحشر",
                                    style: TextStyle(
                                        fontSize: 12
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    ),

                    SizedBox(width: 25),

                    //Black Circle Avatars
                    Column(
                      children: <Widget>[
                        SizedBox(height: 15),

                        CircleAvatar(
                          maxRadius: 4,
                          backgroundColor: Colors.black54,
                        ),

                        SizedBox(height: 7),

                        CircleAvatar(
                          maxRadius: 2,
                          backgroundColor: Colors.black45,
                        ),
                        SizedBox(height: 5),
                        CircleAvatar(
                          maxRadius: 2,
                          backgroundColor: Colors.black45,
                        ),
                        SizedBox(height: 5),
                        CircleAvatar(
                          maxRadius: 2,
                          backgroundColor: Colors.black45,
                        ),
                        SizedBox(height: 5),
                        CircleAvatar(
                          maxRadius: 2,
                          backgroundColor: Colors.black45,
                        ),
                        SizedBox(height: 5),
                        CircleAvatar(
                          maxRadius: 2,
                          backgroundColor: Colors.black45,
                        ),

                        SizedBox(height: 7),

                        CircleAvatar(
                          maxRadius: 4,
                          backgroundColor: Colors.black54,
                        ),
                      ],
                    ),

                    SizedBox(width: 40)

                  ],
                ),

                SizedBox(height: 7),

                Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    RaisedButton(
                        color: Colors.teal[600],
                        child: Text("حفظ",style: TextStyle(
                          color: Colors.white,
                          fontSize: 17,
                        ),),
                        onPressed: () {}
                    ),
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}